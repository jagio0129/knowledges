UIT Meetup vol.18『俺たちが考える最強のパフォーマンス・チューニング』
===

https://uit.connpass.com/event/267776/

## Webパフォーマンスの基礎知識と困難さについて

- 海外のWebマガジン「Smashing Magazine」で「Front-End Performance Checklist」と第してWebパフォーマンスの情報を毎年発表
  - https://github.com/thedaviddias/Front-End-Performance-Checklist
  - https://www.smashingmagazine.com/2021/01/front-end-performance-2021-free-pdf-checklist/
  - 読むのにパフォーマンスの専門知識がいる
  - 英文がクソ長い
  -
- Webパフォーマンスの20%ルール
  - 競合他社より20%早い状態を目標とする
- Webパフォーマンスで利用される指標
  - lighthouseの指標
    - TTI
    - FID
- CoreWebVitals
  - https://webbu.jp/corewebvitals-6076
  - Googleが出した新しいパフォーマンス指標のセット
  - 注意点がかなりあるみたい
  - 指標は定期的に更新される

## APIレスポンスのおてがるキャッシュ化 with Recoil

- [ReactにおけるGlobal stateの管理法4選](https://weseek.co.jp/tech/2565/)
- [【LINE証券 FrontEnd】Recoilを使って安全快適な状態管理を手に入れた話](https://engineering.linecorp.com/ja/blog/line-sec-frontend-using-recoil-to-get-a-safe-and-comfortable-state-management/)
- [React ステート管理 比較考察](https://blog.uhy.ooo/entry/2021-07-24/react-state-management/)

## JSをへらすための10のこと

- https://leader22.github.io/slides/uit_meetup-18/#17
- 年々JSのコード量は増え速度は下がっている。3,40%使われてない。
- どう減らすのか
  - HTMLでいいならそれでやる
- islands architecuture
  - [Island Archtecureとは](https://qiita.com/oekazuma/items/87923acea03990fd71a2)
    - 「ページ内でサーバー側でレンダリングされる静的な部分とインタラクティブなアプリの部分をそれぞれ独立して表示させる手法です。」
    - フレームワークでいうと[Asrtro](https://docs.astro.build/ja/concepts/islands/)
  - パーシャルハイドレーション
