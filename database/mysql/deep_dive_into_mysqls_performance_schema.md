Deep Dive into MySQL’s Performance Schema
===

> [原文](https://www.percona.com/blog/deep-dive-into-mysqls-performance-schema/?utm_campaign=2023%20Blog%20Q1&utm_content=233500726&utm_medium=social&utm_source=twitter&hss_channel=tw-35373186)

> 注意: 原文中では頻繁に「instruments」という単語が出てくるが、ここでは「インストゥルメント」と記載しておく。
> 文脈的には、perfromance schemaで確認できる各項目を際しているように読める

最近、私はある顧客と仕事をしていて、その顧客の複数のMySQLデータベースノードのパフォーマンス監査を行うことに焦点を合わせていました。私たちは、パフォーマンススキーマの統計情報を調べ始めました。作業中、お客様は2つの興味深い質問をされました：パフォーマンススキーマを完全に活用する方法と、必要なものを見つける方法です。私は、パフォーマンス・スキーマの見識を理解し、それを効果的に利用することが重要であることに気づきました。このブログを読めば、誰もが理解しやすくなるはずです。

パフォーマンススキーマはMySQLのエンジンで、SHOW ENGINESで簡単に有効かどうかを確認することができます。これは完全に様々なインストルメント（イベント名とも呼ばれる）のセットで構成されており、それぞれが異なる目的をもっている。

インストゥルメントはパフォーマンススキーマの主要な部分です。問題やその根本的な原因を調査したいときに便利です。以下にいくつかの例を挙げる（ただし、これらに限定されない）:

1. どのIOオペレーションがMySQLの速度を低下させているか？
2. プロセス/スレッドが主に待っているファイルはどれか？
3. どの実行段階でクエリに時間がかかっているか、または alter コマンドにどのくらい時間がかかっているか？
4. どのプロセスが最もメモリを消費しているか、メモリリークの原因を特定するにはどうすればよいか？
5. 複数のインストゥルメントを有効にする方法は、ステージング環境で、発見したことを最適化して、本番に移行するのがベストです。

## What is an instrument in terms of performance schema?

インストルメントとは、wait、io、sql、binlog、fileなど、さまざまなコンポーネントの組み合わせのことである。これらのコンポーネントを組み合わせると、さまざまな問題のトラブルシューティングを支援する有意義なツールとなります。例えば、wait/io/file/sql/binlogは、バイナリログファイルの待機とI/Oの詳細に関する情報を提供するインストゥルメントの1つです。計器は左から読み込まれ、その後、区切り記号「/」でコンポーネントが追加されます。インストゥルメントに追加するコンポーネントが増えるほど、インストゥルメントはより複雑に、より具体的になります。

MySQLのバージョンで利用可能なすべてのインストルメントは、setup_instrumentsテーブルの下に見つけることができます。MySQL のバージョンごとに、インストゥルメントの数が異なることは注目に値します。

```sql
select count(1) from performance_schema.setup_instruments;

+----------+

| count(1) |

+----------+

|     1269 |

+----------+
```

わかりやすいように、楽器は以下のように7つのパートに分けることができます。ここで使用しているMySQLのバージョンは8.0.30です。それ以前のバージョンでは、4つしかありませんでしたので、異なる/低いバージョンを使用している場合には、楽器の種類が異なることを期待してください。


```sql
select distinct(substring_index(name,'/',1)) from performance_schema.setup_instruments;

+-------------------------------+

| (substring_index(name,'/',1)) |

+-------------------------------+

| wait                          |

| idle                          |

| stage                         |

| statement                     |

| transaction                   |

| memory                        |

| error                         |

+-------------------------------+

7 rows in set (0.01 sec)
```

|項目|概要|
|---|---|
|Stage|データの読み込み、データの送信、テーブルの変更、クエリのキャッシュのチェックなど、あらゆるクエリの実行段階を提供します。 例えば、stage/sql/altering tableのようなものです。|
|Wait|ミューテックス待ち、ファイル待ち、I/O待ち、テーブル待ちなど。これに該当する楽器は、wait/io/file/sql/mapになります。|
|Memory|スレッドごとのメモリ使用量に関する情報を提供するインストゥルメント。例： memory/sql/MYSQL_BIN_LOG|
|Statement|SQLの種類やストアドプロシージャの情報を提供します。|
|Idle|ソケット接続に関する情報およびスレッドに関連する情報を提供する。|
|Transaction|トランザクションに関する情報を提供する唯一のインストゥルメント|
|Error|ユーザーの活動によって発生するエラーに関連する情報を得ることができます|

これら7つのコンポーネントの楽器総数を以下に示します。これらの楽器は、これらの名前から始まるものだけを識別することができます。

```sql
select distinct(substring_index(name,'/',1)) as instrument_name,count(1) from performance_schema.setup_instruments group by instrument_name;

+-----------------+----------+

| instrument_name | count(1) |

+-----------------+----------+

| wait            |      399 |

| idle            |        1 |

| stage           |      133 |

| statement       |      221 |

| transaction     |        1 |

| memory          |      513 |

| error           |        1 |

+-----------------+----------+
```


## How to find which instrument you need

以前、お客様から「何千台もの楽器がある中で、どうやって自分の必要な楽器を探せばいいのか」と聞かれたことがあります。先ほど、インストゥルメントは左から右へと読み込まれると言いましたが、どのインストゥルメントが必要なのか、そして、それぞれのパフォーマンスを見つけることができます。

例1 - 私はMySQLインスタンスのREDOログ（ログファイルまたはWALファイル）のパフォーマンスを観察する必要があり、スレッドや接続が次の書き込みの前にREDOログファイルのフラッシュを待つ必要があるかどうか、もしそうならどの程度かを確認する必要があります。

```sql
select * from setup_instruments where name like '%innodb_log_file%';

+-----------------------------------------+---------+-------+------------+------------+---------------+

| NAME                                    | ENABLED | TIMED | PROPERTIES | VOLATILITY | DOCUMENTATION |

+-----------------------------------------+---------+-------+------------+------------+---------------+

| wait/synch/mutex/innodb/log_files_mutex | NO      | NO    |            |          0 | NULL          |

| wait/io/file/innodb/innodb_log_file     | YES     | YES   |            |          0 | NULL          |

+-----------------------------------------+---------+-------+------------+----------
```

ここで、REDOログファイルに対して2つのインストゥルメントがあることがわかります。1つはREDOログファイルのミューテックス統計で、もう1つはREDOログファイルのIO待ち統計です。

例2 - 所要時間（一括更新にかかる時間）を計算できる操作やインストゥルメントを見つける必要があります。以下は、同じものを見つけるのに役立つすべてのインストゥルメントです。

```sql
select * from setup_instruments where PROPERTIES='progress';

+------------------------------------------------------+---------+-------+------------+------------+---------------+

| NAME                                                 | ENABLED | TIMED | PROPERTIES | VOLATILITY | DOCUMENTATION |

+------------------------------------------------------+---------+-------+------------+------------+---------------+

| stage/sql/copy to tmp table                          | YES     | YES   | progress   |          0 | NULL          |

| stage/sql/Applying batch of row changes (write)      | YES     | YES   | progress   |          0 | NULL          |

| stage/sql/Applying batch of row changes (update)     | YES     | YES   | progress   |          0 | NULL          |

| stage/sql/Applying batch of row changes (delete)     | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (end)                       | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (flush)                     | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (insert)                    | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (log apply index)           | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (log apply table)           | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (merge sort)                | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter table (read PK and internal sort) | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/alter tablespace (encryption)           | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/buffer pool load                        | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/clone (file copy)                       | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/clone (redo copy)                       | YES     | YES   | progress   |          0 | NULL          |

| stage/innodb/clone (page copy)                       | YES     | YES   | progress   |          0 | NULL          |

+------------------------------------------------------+---------+-------+------------+------------+-------
```

上記の楽器は、進捗状況を把握できるものです。

## How to prepare these instruments to troubleshoot the performance issues

これらのインストゥルメントを利用するには、まずパフォーマンススキーマのログ関連データを有効にする必要があります。実行中のスレッドの情報をログに記録するだけでなく、そのようなスレッドの履歴（ステートメント/ステージまたは任意の特定の操作）を維持することも可能である。私が使用しているバージョンでは、デフォルトで、いくつのインストゥルメントが有効になっているか見てみましょう。私は他の計器を明示的に有効にしていません。

```sql
select count(*) from setup_instruments where ENABLED='YES';

+----------+

| count(*) |

+----------+

|      810 |

+----------+

1 row in set (0.00 sec)
```

以下のクエリは、ロギングが行われる上位30種類の有効なインストゥルメントをテーブルにリストアップします。

```sql
select * from performance_schema.setup_instruments where enabled='YES' limit 30;


+---------------------------------------+---------+-------+------------+------------+---------------+

| NAME                                  | ENABLED | TIMED | PROPERTIES | VOLATILITY | DOCUMENTATION |

+---------------------------------------+---------+-------+------------+------------+---------------+

| wait/io/file/sql/binlog               | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/binlog_cache         | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/binlog_index         | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/binlog_index_cache   | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/relaylog             | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/relaylog_cache       | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/relaylog_index       | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/relaylog_index_cache | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/io_cache             | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/casetest             | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/dbopt                | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/ERRMSG               | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/select_to_file       | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/file_parser          | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/FRM                  | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/load                 | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/LOAD_FILE            | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/log_event_data       | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/log_event_info       | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/misc                 | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/pid                  | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/query_log            | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/slow_log             | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/tclog                | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/trigger_name         | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/trigger              | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/init                 | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/SDI                  | YES     | YES   |            |          0 | NULL          |

| wait/io/file/sql/hash_join            | YES     | YES   |            |          0 | NULL          |

| wait/io/file/mysys/proc_meminfo       | YES     | YES   |            |          0 | NULL          |

+---------------------------------------+---------+-------+------------+------------+--------------
```

前述したように、イベントの履歴を管理することも可能です。例えば、負荷テストを行っていて、終了後のクエリのパフォーマンスを分析したい場合、以下のコンシューマーがまだ有効になっていなければ、有効にしておく必要があります。

```sql
select * from performance_schema.setup_consumers;

+----------------------------------+---------+

| NAME                             | ENABLED |

+----------------------------------+---------+

| events_stages_current            | YES     |

| events_stages_history            | YES     |

| events_stages_history_long       | YES     |

| events_statements_cpu            | YES     |

| events_statements_current        | YES     |

| events_statements_history        | YES     |

| events_statements_history_long   | YES     |

| events_transactions_current      | YES     |

| events_transactions_history      | YES     |

| events_transactions_history_long | YES     |

| events_waits_current             | YES     |

| events_waits_history             | YES     |

| events_waits_history_long        | YES     |

| global_instrumentation           | YES     |

| thread_instrumentation           | YES     |

| statements_digest                | YES     |

+----------------------------------+---------+
```

> 上の行のトップ15レコードは自明ですが、最後のdigestはSQL文のダイジェストテキストを許可することを意味します。ダイジェストとは、類似のクエリをグループ化し、そのパフォーマンスを表示することを意味します。これはハッシュ化アルゴリズムによって行われています。

例えば、あるクエリで最も時間がかかっているステージを分析したい場合、以下のクエリを使用してそれぞれのロギングを有効にする必要があります。

```sql
MySQL> update performance_schema.setup_consumers set ENABLED='YES' where NAME='events_stages_current';

Query OK, 1 row affected (0.00 sec)

Rows matched: 1  Changed: 1  Warnings: 0
```

## How to take advantage of the performance schema?

さて、インストゥルメントとは何か、インストゥルメントを有効にする方法、保存したいデータの量がわかったところで、これらのインストゥルメントをどのように利用するかを理解することにしましょう。1000以上の計器があるので、すべてを網羅することはできませんが、わかりやすくするために、私のテストケースからいくつかの計器の出力を取り上げてみました。

なお、偽の負荷を生成するために、私は[sysbench](https://github.com/akopytov/sysbench)を使用して、以下の詳細を使用して読み取りと書き込みのトラフィックを作成しました。

```
lua : oltp_read_write.lua

Number of tables : 1

table_Size : 100000

threads : 4/10 

rate - 10
```

例えば、メモリの使用量を調べる場合を考えてみましょう。これを調べるために、メモリに関連するテーブルに対して以下のクエリを実行してみましょう。

```sql
select * from memory_summary_global_by_event_name order by SUM_NUMBER_OF_BYTES_ALLOC desc limit 3\G;



*************************** 1. row ***************************

                  EVENT_NAME: memory/innodb/buf_buf_pool

                 COUNT_ALLOC: 24

                  COUNT_FREE: 0

   SUM_NUMBER_OF_BYTES_ALLOC: 3292102656

    SUM_NUMBER_OF_BYTES_FREE: 0

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 24

             HIGH_COUNT_USED: 24

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 3292102656

   HIGH_NUMBER_OF_BYTES_USED: 3292102656

*************************** 2. row ***************************

                  EVENT_NAME: memory/sql/THD::main_mem_root

                 COUNT_ALLOC: 138566

                  COUNT_FREE: 138543

   SUM_NUMBER_OF_BYTES_ALLOC: 2444314336

    SUM_NUMBER_OF_BYTES_FREE: 2443662928

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 23

             HIGH_COUNT_USED: 98

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 651408

   HIGH_NUMBER_OF_BYTES_USED: 4075056

*************************** 3. row ***************************

                  EVENT_NAME: memory/sql/Filesort_buffer::sort_keys

                 COUNT_ALLOC: 58869

                  COUNT_FREE: 58868

   SUM_NUMBER_OF_BYTES_ALLOC: 2412676319

    SUM_NUMBER_OF_BYTES_FREE: 2412673879

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 1

             HIGH_COUNT_USED: 13

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 2440

   HIGH_NUMBER_OF_BYTES_USED: 491936


Above are the top three records, showing where the memory is getting mostly utilized.
```

インストゥルメント memory/innodb/buf_buf_poolは、3GBを使用しているバッファープールに関連しており、SUM_NUMBER_OF_BYTES_ALLOCからこの情報を取得することができます。もう1つのデータはCURRENT_COUNT_USEDで、これは現在割り当てられたデータのブロック数を示し、作業が完了すると、このカラムの値が変更されることを意味します。このレコードの統計を見ると、MySQLはかなり頻繁にバッファプールを使用するので、3GBの消費は問題ではありません（例えば、データの書き込み、データのロード、データの変更など）。しかし、メモリリークの問題がある場合や、バッファプールが使用されていない場合は、問題が発生します。このような場合、このインストゥルメントは分析に非常に有効です。

2番目のインストゥルメント memory/sql/THD::main_mem_rootを見ると、2Gを利用しており、sqlに関連しています（一番左からこのように読み取ります）。THD::main_mem_rootはスレッドクラスの1つです。この楽器を理解してみましょう。

THDはスレッドを表します。

main_mem_rootはmem_root のクラスです。MEM_ROOT は、クエリの解析時、実行計画時、ネストしたクエリ/サブクエリの実行時、その他クエリ実行中にスレッドにメモリを割り当てるために使用される構造体です。さて、今回のケースでは、どのスレッド/ホストがメモリを消費しているかを確認し、クエリをさらに最適化できるようにしたいのですが、そのためには、どのスレッド/ホストがメモリを消費しているかを確認する必要があります。さらに掘り下げていく前に、まず3番目のインストゥルメントについて理解しましょう。

memory/sql/filesort_buffer::sort_keys - 楽器名は左から読みます。この場合、それはSQLに割り当てられたメモリに関連しています。この楽器の次のコンポーネントはfilesort_buffer::sort_keysで、データのソートを担当します（データが格納され、ソートが必要なバッファである可能性があります。この例としては、インデックスの作成や通常のorder by節などがあります。）

どの接続がこのメモリを使用しているのか、掘り下げて分析する時が来ました。  これを調べるために、memory_summary_by_host_by_event_nameテーブルを使い、アプリケーションサーバーから来るレコードをフィルタリングしています。

```sql
select * from memory_summary_by_host_by_event_name where HOST='10.11.120.141' order by SUM_NUMBER_OF_BYTES_ALLOC desc limit 2\G;

*************************** 1. row ***************************

                        HOST: 10.11.120.141

                  EVENT_NAME: memory/sql/THD::main_mem_root

                 COUNT_ALLOC: 73817

                  COUNT_FREE: 73810

   SUM_NUMBER_OF_BYTES_ALLOC: 1300244144

    SUM_NUMBER_OF_BYTES_FREE: 1300114784

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 7

             HIGH_COUNT_USED: 39

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 129360

   HIGH_NUMBER_OF_BYTES_USED: 667744

*************************** 2. row ***************************

                        HOST: 10.11.120.141

                  EVENT_NAME: memory/sql/Filesort_buffer::sort_keys

                 COUNT_ALLOC: 31318

                  COUNT_FREE: 31318

   SUM_NUMBER_OF_BYTES_ALLOC: 1283771072

    SUM_NUMBER_OF_BYTES_FREE: 1283771072

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 0

             HIGH_COUNT_USED: 8

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 0

   HIGH_NUMBER_OF_BYTES_USED: 327936
```

イベント名 memory/sql/THD::main_mem_root は、このクエリ実行時に私のアプリケーション・ホストであるホスト 11.11.120.141 によって 1G 以上のメモリ ( 合計 ) を消費しています。このホストがメモリを消費していることがわかったので、さらに掘り下げてネストやサブクエリなどのクエリを見つけ、それを最適化することができます。

同様に、filesort_buffer::sort_keysによるメモリ割り当てが、実行時に1G（合計）以上であることを確認します。このようなインストゥルメントは、ソートすなわちorder by句を使用したクエリを参照するようにとの合図です。

## Time to join all dotted lines

ファイルソートによってメモリの大部分が使用されているケースで、犯人となるスレッドを探し出してみましょう。 最初のクエリは、ホストとイベント名（インストゥルメント）を見つけるのに役立ちます。

```sql
select * from memory_summary_by_host_by_event_name order by SUM_NUMBER_OF_BYTES_ALLOC desc limit 1\G;

*************************** 1. row ***************************

                        HOST: 10.11.54.152

                  EVENT_NAME: memory/sql/Filesort_buffer::sort_keys

                 COUNT_ALLOC: 5617297

                  COUNT_FREE: 5617297

   SUM_NUMBER_OF_BYTES_ALLOC: 193386762784

    SUM_NUMBER_OF_BYTES_FREE: 193386762784

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 0

             HIGH_COUNT_USED: 20

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 0

   HIGH_NUMBER_OF_BYTES_USED: 819840
```

これは私のアプリケーションホストです。どのユーザーが実行しているか、それぞれのスレッドIDを調べてみましょう。

```sql
select * from memory_summary_by_account_by_event_name where HOST='10.11.54.152' order by SUM_NUMBER_OF_BYTES_ALLOC desc limit 1\G;

*************************** 1. row ***************************

                        USER: sbuser

                        HOST: 10.11.54.152

                  EVENT_NAME: memory/sql/Filesort_buffer::sort_keys

                 COUNT_ALLOC: 5612993

                  COUNT_FREE: 5612993

   SUM_NUMBER_OF_BYTES_ALLOC: 193239513120

    SUM_NUMBER_OF_BYTES_FREE: 193239513120

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 0

             HIGH_COUNT_USED: 20

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 0

   HIGH_NUMBER_OF_BYTES_USED: 819840




select * from memory_summary_by_thread_by_event_name where EVENT_NAME='memory/sql/Filesort_buffer::sort_keys' order by SUM_NUMBER_OF_BYTES_ALLOC desc limit 1\G;

*************************** 1. row ***************************

                   THREAD_ID: 84

                  EVENT_NAME: memory/sql/Filesort_buffer::sort_keys

                 COUNT_ALLOC: 565645

                  COUNT_FREE: 565645

   SUM_NUMBER_OF_BYTES_ALLOC: 19475083680

    SUM_NUMBER_OF_BYTES_FREE: 19475083680

              LOW_COUNT_USED: 0

          CURRENT_COUNT_USED: 0

             HIGH_COUNT_USED: 2

    LOW_NUMBER_OF_BYTES_USED: 0

CURRENT_NUMBER_OF_BYTES_USED: 0

   HIGH_NUMBER_OF_BYTES_USED: 81984
```

これで、ユーザーとスレッド ID の完全な詳細が得られました。このスレッドでどのような種類のクエリーが実行されているか見てみましょう。

```sql
select * from events_statements_history where THREAD_ID=84 order by SORT_SCAN desc\G;

*************************** 1. row ***************************

              THREAD_ID: 84

               EVENT_ID: 48091828

           END_EVENT_ID: 48091833

             EVENT_NAME: statement/sql/select

                 SOURCE: init_net_server_extension.cc:95

            TIMER_START: 145083499054314000

              TIMER_END: 145083499243093000

             TIMER_WAIT: 188779000

              LOCK_TIME: 1000000

               SQL_TEXT: SELECT c FROM sbtest2 WHERE id BETWEEN 5744223 AND 5744322 ORDER BY c

                 DIGEST: 4f764af1c0d6e44e4666e887d454a241a09ac8c4df9d5c2479f08b00e4b9b80d

            DIGEST_TEXT: SELECT `c` FROM `sbtest2` WHERE `id` BETWEEN ? AND ? ORDER BY `c`

         CURRENT_SCHEMA: sysbench

            OBJECT_TYPE: NULL

          OBJECT_SCHEMA: NULL

            OBJECT_NAME: NULL

  OBJECT_INSTANCE_BEGIN: NULL

            MYSQL_ERRNO: 0

      RETURNED_SQLSTATE: NULL

           MESSAGE_TEXT: NULL

                 ERRORS: 0

               WARNINGS: 0

          ROWS_AFFECTED: 0

              ROWS_SENT: 14

          ROWS_EXAMINED: 28

CREATED_TMP_DISK_TABLES: 0

     CREATED_TMP_TABLES: 0

       SELECT_FULL_JOIN: 0

 SELECT_FULL_RANGE_JOIN: 0

           SELECT_RANGE: 1

     SELECT_RANGE_CHECK: 0

            SELECT_SCAN: 0

      SORT_MERGE_PASSES: 0

         SORT_RANGE: 0

              SORT_ROWS: 14

          SORT_SCAN: 1

          NO_INDEX_USED: 0

     NO_GOOD_INDEX_USED: 0

       NESTING_EVENT_ID: NULL

     NESTING_EVENT_TYPE: NULL

    NESTING_EVENT_LEVEL: 0

           STATEMENT_ID: 49021382

               CPU_TIME: 185100000

       EXECUTION_ENGINE: PRIMARY
```

ここでは、rows_scan（テーブルスキャンのこと）に従って1つのレコードだけを貼り付けましたが、あなたのケースで同様の他のクエリを見つけ、インデックスを作成するか、他の適切なソリューションによって最適化することができます。

### Example Two
ユーザー・テーブルに対して、読み取りロック、書き込みロックなど、どのロックが何秒間かかっているか（ピコ秒単位で表示）、テーブル・ロックの状況を調べてみましょう。

書き込みロックでテーブルをロックする:

```sql
Shell
mysql> lock tables sbtest2 write;

Query OK, 0 rows affected (0.00 sec)
```

```sql
mysql> show processlist;

+----+--------+---------------------+--------------------+-------------+--------+-----------------------------------------------------------------+------------------+-----------+-----------+---------------+

| Id | User   | Host                | db                 | Command     | Time   | State                                                           | Info             | Time_ms   | Rows_sent | Rows_examined |

+----+--------+---------------------+--------------------+-------------+--------+-----------------------------------------------------------------+------------------+-----------+-----------+---------------+

|  8 | repl   | 10.11.139.171:53860 | NULL               | Binlog Dump | 421999 | Source has sent all binlog to replica; waiting for more updates | NULL             | 421998368 |         0 |             0 |

|  9 | repl   | 10.11.223.98:51212  | NULL               | Binlog Dump | 421998 | Source has sent all binlog to replica; waiting for more updates | NULL             | 421998262 |         0 |             0 |

| 25 | sbuser | 10.11.54.152:38060  | sysbench           | Sleep       |  65223 |                                                                 | NULL             |  65222573 |         0 |             1 |

| 26 | sbuser | 10.11.54.152:38080  | sysbench           | Sleep       |  65222 |                                                                 | NULL             |  65222177 |         0 |             1 |

| 27 | sbuser | 10.11.54.152:38090  | sysbench           | Sleep       |  65223 |                                                                 | NULL             |  65222438 |         0 |             0 |

| 28 | sbuser | 10.11.54.152:38096  | sysbench           | Sleep       |  65223 |                                                                 | NULL             |  65222489 |         0 |             1 |

| 29 | sbuser | 10.11.54.152:38068  | sysbench           | Sleep       |  65223 |                                                                 | NULL             |  65222527 |         0 |             1 |

| 45 | root   | localhost           | performance_schema | Sleep       |   7722 |                                                                 | NULL             |   7722009 |        40 |           348 |

| 46 | root   | localhost           | performance_schema | Sleep       |   6266 |                                                                 | NULL             |   6265800 |        16 |          1269 |

| 47 | root   | localhost           | performance_schema | Sleep       |   4904 |                                                                 | NULL             |   4903622 |         0 |            23 |

| 48 | root   | localhost           | performance_schema | Sleep       |   1777 |                                                                 | NULL             |   1776860 |         0 |             0 |

| 54 | root   | localhost           | sysbench           | Sleep       |    689 |                                                                 | NULL             |    688740 |         0 |             1 |

| 58 | root   | localhost           | NULL               | Sleep       |     44 |                                                                 | NULL             |     44263 |         1 |             1 |

| 59 | root   | localhost           | sysbench           | Query       |      0 | init                                                            | show processlist |         0 |         0 |             0 |

+----+--------+---------------------+--------------------+-------------+--------+-----------------
```

さて、このセッションに気づかず、このテーブルを読もうとして、[メタデータ](https://dev.mysql.com/doc/refman/8.0/ja/metadata-locking.html)のロックを待っている状況を考えてみましょう。この場合、ロックに関連するインストゥルメント、つまり wait/table/lock/sql/handler (table_handles はテーブルロックインストゥルメントを担当するテーブルです) の助けを借りる必要があります（どのセッションがこのテーブルをロックしているのかを知るため）。

```sql
mysql> select * from table_handles where object_name='sbtest2' and OWNER_THREAD_ID is not null;

+-------------+---------------+-------------+-----------------------+-----------------+----------------+---------------+----------------+

| OBJECT_TYPE | OBJECT_SCHEMA | OBJECT_NAME | OBJECT_INSTANCE_BEGIN | OWNER_THREAD_ID | OWNER_EVENT_ID | INTERNAL_LOCK | EXTERNAL_LOCK  |

+-------------+---------------+-------------+-----------------------+-----------------+----------------+---------------+----------------+

| TABLE       | sysbench      | sbtest2     |       140087472317648 |             141 |             77 | NULL          | WRITE EXTERNAL |

+-------------+---------------+-------------+-----------------------+-----------------+----------------+---------------+----------------+
```

```sql
mysql> select * from metadata_locks;

+---------------+--------------------+------------------+-------------+-----------------------+----------------------+---------------+-------------+-------------------+-----------------+----------------+

| OBJECT_TYPE   | OBJECT_SCHEMA      | OBJECT_NAME      | COLUMN_NAME | OBJECT_INSTANCE_BEGIN | LOCK_TYPE            | LOCK_DURATION | LOCK_STATUS | SOURCE            | OWNER_THREAD_ID | OWNER_EVENT_ID |

+---------------+--------------------+------------------+-------------+-----------------------+----------------------+---------------+-------------+-------------------+-----------------+----------------+

| GLOBAL        | NULL               | NULL             | NULL        |       140087472151024 | INTENTION_EXCLUSIVE  | STATEMENT     | GRANTED     | sql_base.cc:5534  |             141 |             77 |

| SCHEMA        | sysbench           | NULL             | NULL        |       140087472076832 | INTENTION_EXCLUSIVE  | TRANSACTION   | GRANTED     | sql_base.cc:5521  |             141 |             77 |

| TABLE         | sysbench           | sbtest2          | NULL        |       140087471957616 | SHARED_NO_READ_WRITE | TRANSACTION   | GRANTED     | sql_parse.cc:6295 |             141 |             77 |

| BACKUP TABLES | NULL               | NULL             | NULL        |       140087472077120 | INTENTION_EXCLUSIVE  | STATEMENT     | GRANTED     | lock.cc:1259      |             141 |             77 |

| TABLESPACE    | NULL               | sysbench/sbtest2 | NULL        |       140087471954800 | INTENTION_EXCLUSIVE  | TRANSACTION   | GRANTED     | lock.cc:812       |             141 |             77 |

| TABLE         | sysbench           | sbtest2          | NULL        |       140087673437920 | SHARED_READ          | TRANSACTION   | PENDING     | sql_parse.cc:6295 |             142 |             77 |

| TABLE         | performance_schema | metadata_locks   | NULL        |       140088117153152 | SHARED_READ          | TRANSACTION   | GRANTED     | sql_parse.cc:6295 |             143 |            970 |

| TABLE         | sysbench           | sbtest1          | NULL        |       140087543861792 | SHARED_WRITE         | TRANSACTION   | GRANTED     | sql_parse.cc:6295 |             132 |            156 |

+---------------+--------------------+------------------+-------------+-----------------------+-
```

このことから、スレッド ID 141 が sbtest2 のロック "SHARED_NO_READ_WRITE" を保持していることがわかります。したがって、その要件を理解したら、正しい手順、つまりセッションをコミットするか強制終了するかを決めることができます。スレッドテーブルからそれぞれのprocesslist_idを見つけ、それをkillする必要があります。

```sql
mysql> kill 63;

Query OK, 0 rows affected (0.00 sec)
```

```sql
mysql> select * from table_handles where object_name='sbtest2' and OWNER_THREAD_ID is not null;

Empty set (0.00 sec)
```

### Example Three

状況によっては、MySQLサーバーがどこで待ち時間を過ごしているかを調べて、さらなる対策を講じる必要があります。

```sql
mysql> select * from events_waits_history order by TIMER_WAIT desc limit 2\G;

*************************** 1. row ***************************

            THREAD_ID: 88

             EVENT_ID: 124481038

         END_EVENT_ID: 124481038

           EVENT_NAME: wait/io/file/sql/binlog

               SOURCE: mf_iocache.cc:1694

          TIMER_START: 356793339225677600

            TIMER_END: 420519408945931200

           TIMER_WAIT: 63726069720253600

                SPINS: NULL

        OBJECT_SCHEMA: NULL

          OBJECT_NAME: /var/lib/mysql/mysqld-bin.000009

           INDEX_NAME: NULL

          OBJECT_TYPE: FILE

OBJECT_INSTANCE_BEGIN: 140092364472192

     NESTING_EVENT_ID: 124481033

   NESTING_EVENT_TYPE: STATEMENT

            OPERATION: write

      NUMBER_OF_BYTES: 683

                FLAGS: NULL

*************************** 2. row ***************************

            THREAD_ID: 142

             EVENT_ID: 77

         END_EVENT_ID: 77

           EVENT_NAME: wait/lock/metadata/sql/mdl

               SOURCE: mdl.cc:3443

          TIMER_START: 424714091048155200

            TIMER_END: 426449252955162400

           TIMER_WAIT: 1735161907007200

                SPINS: NULL

        OBJECT_SCHEMA: sysbench

          OBJECT_NAME: sbtest2

           INDEX_NAME: NULL

          OBJECT_TYPE: TABLE

OBJECT_INSTANCE_BEGIN: 140087673437920

     NESTING_EVENT_ID: 76

   NESTING_EVENT_TYPE: STATEMENT

            OPERATION: metadata lock

      NUMBER_OF_BYTES: NULL

                FLAGS: NULL

2 rows in set (0.00 sec)
```

上記の例では、binログファイルはmysqld-bin.000009でIO処理を行うためにほとんどの時間（timer_waitはpico秒）待機しています。これは、ストレージが一杯になっているなど、いくつかの理由が考えられます。次の記録は、以前説明した例2の詳細を示しています。

## What else?

これらのインストゥルメントをより便利に、より簡単に監視するために、[Percona Monitoring and Management (PMM)](https://docs.percona.com/percona-monitoring-and-management/index.html) が重要な役割を果たします。例えば、以下のスナップショットをご覧ください。

![img](https://www.percona.com/blog/wp-content/uploads/2023/01/pmm1_blog-1024x206.png)

![img](https://www.percona.com/blog/wp-content/uploads/2023/01/pmm2blog-1024x210.png)

ほとんどの計器を設定することができ、問い合わせの代わりに、これらのグラフを利用することができます。[PMMのデモ](https://pmm2demo.percona.com/graph/d/mysql-performance-schema/mysql-performance-schema-details?from=now-12h&to=now&var-interval=$__auto_interval_interval&var-environment=All&var-node_name=&var-crop_host=&var-service_name=pxc57-2-mysql&var-region=&var-cluster=PXCCluster1&var-node_id=&var-agent_id=&var-service_id=%2Fservice_id%2F03d49df4-3870-460b-a5e4-94647b56a99d&var-az=&var-node_type=All&var-node_model=&var-replication_set=All&var-version=&var-service_type=All&var-database=All&var-username=All&var-schema=All&orgId=1&refresh=1m)をご覧ください。

もちろん、パフォーマンススキーマを知ることはとても役に立ちますが、すべてのスキーマを有効にすると追加コストが発生し、パフォーマンスにも影響を及ぼします。したがって、多くの場合、[Percona Toolkit](https://docs.percona.com/percona-toolkit/)はDBの性能に影響を与えることなく役に立ちます。例えば、pt-index-usage, pt-online schema change, pt-query-digest などです。

### Some important points

1. 履歴テーブルの読み込みは瞬時に行われず、しばらくしてから行われます。スレッドアクティビティが完了した後のみ。
2. すべてのインストゥルメントを有効にすると、インメモリテーブルへの書き込みが増えるため、MySQLのパフォーマンスに影響を与える可能性があります。また、予算的にも負担が大きくなります。したがって、要件に応じてのみ有効にしてください。
3. PMMにはほとんどのインストゥルメントが含まれており、お客様のご要望に応じてさらに多くのインストゥルメントを構成することも可能です。
4. すべてのテーブルの名前を覚えておく必要はありません。PMMを使うか、結合を使ってクエリを作成すればよいのです。この記事では、読者が理解できるように、全体の概念をより小さなチャンクに要約し、その結果、結合を使用しませんでした。
5. 複数の楽器を有効にする方法は、ステージング環境で、発見したことを最適化して、本番に移行するのがベストです。

# Conclusion

パフォーマンススキーマは、MySQL サーバの動作をトラブルシューティングする際に非常に役に立ちます。どのスキーマが必要かを確認する必要があります。もし、まだパフォーマンスについて悩んでいるようでしたら、遠慮なく弊社までご連絡ください。
